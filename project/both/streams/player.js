/**
 * Created by vincilbishop on 3/17/15.
 */

PlayerStream = new Meteor.Stream('playerStream');

if (Meteor.isServer) {
  PlayerStream.permissions.write(function(eventName) {
    return true;
  });

  PlayerStream.permissions.read(function(eventName) {
    return true;
  });
}
